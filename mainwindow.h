#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "runningprocessthread.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

private slots:
  void handleButtonClick();
  void handleNewMessage(const QString &message);


private:
  Ui::MainWindow *ui;
  RunningProcessThread *mRunningProcessThread;
};

#endif // MAINWINDOW_H
