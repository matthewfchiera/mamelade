#include "runningprocessthread.h"

RunningProcessThread::RunningProcessThread(QObject* parent)
  : QThread(parent)
  , mDispose(false)
{
  connect(this, SIGNAL(finished()), this, SLOT(onRunThreadFinished()));
}

RunningProcessThread::~RunningProcessThread()
{
  mDispose = true;
  this->quit(); // Quit the event loop if it was running.
  this->wait();
}

void RunningProcessThread::startProcessRunning(const QString& command, const QStringList& args)
{
  if (!this->wait(500))
  {
    emit newMessage(QString("Can't start a process because because there is one already running: "
                            "Currently running '%1'").arg(mCommand));
    return;
  }

  mCommand = command;
  mArgs = args;

  emit newMessage(QString("********** Launching: \t'%1'\t**********").arg(mCommand));

  start();
}

void RunningProcessThread::onProcessFinished()
{
  emit newMessage(QString("********** Exiting: \t'%1'\t**********").arg(mCommand));
  this->quit();
}

void RunningProcessThread::onProcessReadyRead()
{
  QProcess *process = dynamic_cast<QProcess*>(sender());
  QByteArray data = process->readAll();

  emit newMessage(data.data());
}

void RunningProcessThread::onRunThreadFinished()
{
  mCommand.clear();
  mArgs.clear();
}

void RunningProcessThread::run()
{
  if (mCommand.isEmpty())
    return;

  // Note, disposing the QProcess will kill the process running.
  QProcess process;
  bool isProcessRunning = false;
  process.setProcessChannelMode(QProcess::MergedChannels);

  connect(&process, SIGNAL(finished(int)), this, SLOT(onProcessFinished()));
  connect(&process, SIGNAL(readyRead()), this, SLOT(onProcessReadyRead()));

  process.setProgram(mCommand);
  process.setArguments(mArgs);
  process.start();
  isProcessRunning = process.waitForStarted();

  if (!mDispose && isProcessRunning)
  {
    exec(); // The new thread will wait here in this new event loop until the quit() function is called.
    // quit() is called by one of the following:
    // 1. by the destructor (in which case, mDispose is set to true)
    // 2. by the onProcessFinished event handler (in which case process.state() == QProcess::NotRunning)
  }

  if (process.state() != QProcess::NotRunning)
  {
    process.terminate();
    process.waitForFinished();
  }

}

