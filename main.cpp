#include "mainwindow.h"
#include "configfile.h"

#include <QApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  MainWindow w;
  w.show();

  ConfigFile configFile;
  configFile.parseFolder("D:/Downloads/MAME");


  qDebug() << configFile.getItems().size();

  for (ConfigFile::List::const_iterator item = configFile.getItems().cbegin(); item != configFile.getItems().cend(); ++item)
  {
    qDebug() << *item;
  }


  return a.exec();
}
