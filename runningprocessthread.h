#ifndef RUNNINGPROCESSTHREAD_H
#define RUNNINGPROCESSTHREAD_H

#include <QObject>
#include <QThread>
#include <QProcess>
#include <QString>

class RunningProcessThread : public QThread
{
  Q_OBJECT

public:
  RunningProcessThread(QObject* parent);
  ~RunningProcessThread();

  void startProcessRunning(const QString &command, const QStringList &args = QStringList());

signals:
  void newMessage(const QString &message);

private slots:
  void onProcessFinished();
  void onProcessReadyRead();
  void onRunThreadFinished();

  // QThread interface
private:
  /**
   * @brief run Is the seperate thread to manage the Operator GUI Application process.
   * Once this run is complete, the Operator GUI Application will be shut down,
   * and the Control Authority will be released back into the pool.
   */
  void run();

private:
  QString mCommand;
  QStringList mArgs;
  bool mDispose;

};

#endif // RUNNINGPROCESSTHREAD_H
