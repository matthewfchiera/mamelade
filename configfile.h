#ifndef CONFIGFILE_H
#define CONFIGFILE_H

#include <QString>
#include <QByteArray>
#include <QDebug>

#include <vector>

class ConfigFile
{
public:
  struct Item
  {
    QString Name;
    QString Company;
    QString Date;
    QString Archive;
    QString ParentName;
    QString ParentArchive;
  };

  typedef std::vector<Item> List;
  friend QDebug operator<< (QDebug d, const ConfigFile::Item& item);

  static const char* FIELD_ID_NAME;
  static const char* FIELD_ID_COMPANY;
  static const char* FIELD_ID_DATE;
  static const char* FIELD_ID_ARCHIVE;
  static const char* FIELD_ID_PARENT_NAME;
  static const char* FIELD_ID_PARENT_ARCHIVE;

  ConfigFile();
  ~ConfigFile();

  const List& getItems() const { return mItems; }
  void clearItems() { mItems.clear(); }

  void parseFolder(QString folder);
  void parseFile(const QString& file);

  static bool parseFile(const QString& fileName, Item& output);
  static QString extractField(const char* fieldName, const char* line);

private:
  List mItems;
};

#endif // CONFIGFILE_H
