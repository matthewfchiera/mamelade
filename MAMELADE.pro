#-------------------------------------------------
#
# Project created by QtCreator 2018-04-19T15:23:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MAMELADE
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    runningprocessthread.cpp \
    configfile.cpp

HEADERS  += mainwindow.h \
    runningprocessthread.h \
    configfile.h

FORMS    += mainwindow.ui
