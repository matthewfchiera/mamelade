#include "configfile.h"

#include <QDir>
#include <QString>
#include <QStringList>

const char* ConfigFile::FIELD_ID_NAME = "Name:";
const char* ConfigFile::FIELD_ID_COMPANY = "Company:";
const char* ConfigFile::FIELD_ID_DATE = "Year:";
const char* ConfigFile::FIELD_ID_ARCHIVE = "Archive:";
const char* ConfigFile::FIELD_ID_PARENT_NAME = "Parent Name:";
const char* ConfigFile::FIELD_ID_PARENT_ARCHIVE = "Parent Archive:";

QDebug operator<<(QDebug d, const ConfigFile::Item& item)
{
  d << ConfigFile::FIELD_ID_NAME << item.Name;
  d << ConfigFile::FIELD_ID_COMPANY << item.Company;
  d << ConfigFile::FIELD_ID_DATE << item.Date;
  d << ConfigFile::FIELD_ID_ARCHIVE << item.Archive;
  d << ConfigFile::FIELD_ID_PARENT_NAME << item.ParentName;
  d << ConfigFile::FIELD_ID_PARENT_ARCHIVE << item.ParentArchive;

  return d;
}

ConfigFile::ConfigFile()
{

}

ConfigFile::~ConfigFile()
{
}

void ConfigFile::parseFolder(QString folder)
{
  static const QStringList FILTER("*.txt");

  QDir dir(folder);

  if (dir.exists() == false)
  {
    // The directory does not exist, so return
    qDebug() << __FUNCTION__ << "The directory does not exist:" << dir.absolutePath();
    return;
  }

  QStringList files = dir.entryList(FILTER, QDir::Files);

  if (files.size() == 0)
  {
    // The directory does not have any *.txt files, so return
    qDebug() << __FUNCTION__ << "No *.txt files in directory:" << dir.absolutePath();
    return;
  }

  for (QStringList::ConstIterator file = files.cbegin(); file != files.cend(); ++file)
  {
    parseFile(dir.absoluteFilePath(*file));
  }
}

void ConfigFile::parseFile(const QString& file)
{ 
  ConfigFile::Item item;
  if (parseFile(file, item))
  {
    mItems.push_back(item);
  }
}

bool ConfigFile::parseFile(const QString& fileName, ConfigFile::Item& output)
{
  static const qint64 MAXLEN = 1024;

  QFile file(fileName);
  bool ret = false;

  if (file.exists())
  {
    char data[MAXLEN];
    QString field;

    file.open(QIODevice::ReadOnly);

    while (file.readLine(data, MAXLEN) != -1)
    {
      field = extractField(FIELD_ID_NAME, data);
      if (field.length() > 0)
      {
        output.Name = field;
        ret = true;
        continue;
      }

      field = extractField(FIELD_ID_COMPANY, data);
      if (field.length() > 0)
      {
        output.Company = field;
        continue;
      }

      field = extractField(FIELD_ID_DATE, data);
      if (field.length() > 0)
      {
        output.Date = field;
        continue;
      }

      field = extractField(FIELD_ID_ARCHIVE, data);
      if (field.length() > 0)
      {
        output.Archive = field;
        continue;
      }

      field = extractField(FIELD_ID_PARENT_NAME, data);
      if (field.length() > 0)
      {
        output.ParentName = field;
        continue;
      }

      field = extractField(FIELD_ID_PARENT_ARCHIVE, data);
      if (field.length() > 0)
      {
        output.ParentArchive = field;
        continue;
      }
    }

    file.close();
  }

  return ret;
}

QString ConfigFile::extractField(const char* fieldName, const char* line)
{
  QString value;
  QString lineStr(line);

  if (lineStr.startsWith(fieldName))
  {
    value.append(line);
    value.remove(0, strlen(fieldName));
    value = value.trimmed();
  }

  return value;
}


