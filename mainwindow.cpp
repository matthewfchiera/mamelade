#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  mRunningProcessThread = new RunningProcessThread(this);

  connect(mRunningProcessThread, SIGNAL(newMessage(QString)), this, SLOT(handleNewMessage(QString)));

  connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(handleButtonClick()));
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::handleButtonClick()
{
  mRunningProcessThread->startProcessRunning("notepad++");
}

void MainWindow::handleNewMessage(const QString& message)
{
  ui->textBrowser->append(message);

}
